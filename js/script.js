$(document).ready(function () {

    $(document).on('click', '.desktopMenu a[link] ', function (e) {
        e.preventDefault();
        var target=$(this).attr('link');
        var offset = $(target).offset().top - 100;
        $('html, body').animate({
            scrollTop: offset
        }, 200); 
    });

    $(document).on('click', '.menuAdaptive a[link] ', function (e) {
        e.preventDefault();
        var target=$(this).attr('link');
        var offset = $(target).offset().top - 85;
        $('html, body').animate({
            scrollTop: offset
        }, 200); 
    });

    $(document).on('click', 'a[href="#feedback"]', function (e) {
        e.preventDefault();;
        var offset = $('#feedback').offset().top - 250;
        $('html, body').animate({
            scrollTop: offset
        }, 200); 
    });

    $(document).on('click', '.menuBtn ', function () {
        $('.menuAdaptive').animate({
            top: '85px'
        }, 200);
        $(this).addClass('menuCloseBtn ');
        $(this).removeClass('menuBtn ');
    });
    $(document).on('click', '.menuCloseBtn ', function () {
        $('.menuAdaptive').animate({
            top: '-100vh'
        }, 200);
        $(this).addClass('menuBtn ');
        $(this).removeClass('menuCloseBtn ');
    });
    $(document).on('click', '.mobMenu li a ', function () {
        $('.menuAdaptive').animate({
            top: '-100vh'
        }, 200);
        $('.menuCloseBtn').addClass('menuBtn ');
        $('.menuCloseBtn').removeClass('menuCloseBtn ');
    });

    function activeMenu(){
        if ($(document).scrollTop() != 0) {
            $("header").addClass('headerScroll');
        } else {
            $("header").removeClass('headerScroll');
        }

        $('.activeMenu').removeClass('activeMenu');
        var offset = $(document).scrollTop() + 105;
        var about = $('.about').offset().top;
        var services = $('.services').offset().top;
        var projects = $('.projects').offset().top;
        var jobs = $('.jobs').offset().top;
        var contacts = $('footer').offset().top;

        if (offset >= $(document).height()-$(window).height() ) {
            $('[link="#contacts"]').parent('li').addClass('activeMenu');
        } else if (offset > jobs) {
            $('[link="#jobs"]').parent('li').addClass('activeMenu');
        } else if (offset > projects) {
            $('[link="#projects"]').parent('li').addClass('activeMenu');
        } else if (offset > services) {
            $('[link="#services"]').parent('li').addClass('activeMenu');
        } else if (offset >= about) {
            $('[link="#about"]').parent('li').addClass('activeMenu');
        }
        
        $('.parallax__layer').each(function(){
            var offset = $(document).scrollTop() + 105;
            var parentHeight=$(this).closest('section').height();
            var parentOffset=$(this).closest('section').offset().top;
            percent=(offset-parentOffset)/parentHeight*100;
            offset=20+percent/3+parseInt($(this).attr('data-offset'));
            $(this).css('top', offset+'%');
        })
    }
    activeMenu();
    $(window).scroll(function () {
        activeMenu();
    });



    $(document).on('click', '.tab.btn', function () {
        if ($(this).hasClass('l2')) {
            $(this).closest('.tabs').find('.tabActive.l2').removeClass('tabActive');
            $(this).closest('.tabs').find('.tabContentActive.l2').removeClass('tabContentActive');
            $(this).addClass('tabActive');
            var target = $(this).attr('data-tab');
            $(this).closest('.tabs').find('.tabContent[data-tab="' + target + '"].l2').addClass('tabContentActive');
        } else {
            $(this).closest('.tabs').find('.tabActive:not(.l2)').removeClass('tabActive');
            $(this).closest('.tabs').find('.tabContentActive:not(.l2)').removeClass('tabContentActive');
            $(this).addClass('tabActive');
            var target = $(this).attr('data-tab');
            $(this).closest('.tabs').find('.tabContent[data-tab="' + target + '"]:not(.l2)').addClass('tabContentActive');
        }
    });


    if($('.sliderControls-counter').length>0){
        $('.sliderControls-counter').each(function(){
            var slidesCount=$(this).closest('.sliderWrap').find('.slide').length;
            $(this).find('.count').html(slidesCount);
        });
    }

    $('.aboutSlider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendArrows: '.aboutSliderWrap .sliderControls-arrows',
    });
    $('.aboutSlider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $(this).closest('.sliderWrap').find('.number').html(nextSlide+1);
      });
    $('.projectsSlider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendArrows: '.projectsSliderWrap .sliderControls-arrows',
        dots: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    arrows:false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    variableWidth: false,
                    arrows:false
                }
            }
        ]
    });
    $('.partnersSlider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 6,
        appendArrows: '.partners .sliderControls-arrows.descArrows',
        dots: true,
    });
    $('.partnersSliderMob').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        rows: 2,
        slidesPerRow: 3,
        appendArrows: '.partners .sliderControls-arrows.mobArrows',
        dots: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows:false,
                    rows: 9,
                    slidesPerRow: 2,
                    dots: false
                }
            }
        ]
    });
    
    setTimeout(function(){
        $('#preloader').slideUp(200);
    }, 1000);
});
